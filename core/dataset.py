#! /usr/bin/env python
# coding=utf-8
#================================================================
#   Copyright (C) 2019 * Ltd. All rights reserved.
#
#   Editor      : VIM
#   File name   : dataset.py
#   Author      : YunYang1994
#   Created date: 2019-03-15 18:05:03
#   Description :
#
#================================================================

import os
import cv2
import random
import numpy as np
import tensorflow as tf
import core.utils as utils
from core.config import cfg
from core.data_aug import *
from PIL import Image

def euclide_distance(p1, p2):
    return np.sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))

def find_coeffs(source_coords, target_coords):
    matrix = []
    for s, t in zip(source_coords, target_coords):
        matrix.append([t[0], t[1], 1, 0, 0, 0, -s[0]*t[0], -s[0]*t[1]])
        matrix.append([0, 0, 0, t[0], t[1], 1, -s[1]*t[0], -s[1]*t[1]])
    A = np.matrix(matrix, dtype=np.float)
    B = np.array(source_coords).reshape(8)
    res = np.dot(np.linalg.inv(A.T * A) * A.T, B)
    return np.array(res).reshape(8)

def find_covered_rectangle(box):
    m = np.min(box, axis=0)
    xmin = min(m[0], m[2])
    ymin = min(m[1], m[3])

    m = np.max(box, axis=0)
    xmax = max(m[0], m[2])
    ymax = max(m[1], m[3])

    return (int(xmin), int(ymin), int(xmax), int(ymax))

def find_card_coordinates(box):
    box = np.array(sorted(box, key = lambda b: b[4]))
    x = (box[:, 0] + box[:, 2]) / 2
    y = (box[:, 1] + box[:, 3]) / 2
    center = [(i, j) for (i, j) in zip(x, y)]
    center = np.array(center, dtype=np.int32)
    return center

def get_box(image_size, coordinate, class_name, box_size):
    int_x = round(float(coordinate[0]))
    int_y = round(float(coordinate[1]))

    box = []
    # x1
    if int_x - box_size/2 < 0:
        box.append(0)
    else:
        box.append(int(int_x - box_size/2))

    # y1
    if int_y - box_size/2 < 0:
        box.append(0)
    else:
        box.append(int(int_y - box_size/2))

    # x2
    if int_x + box_size/2 > int(image_size[0]):
        box.append(image_size[0])
    else:
        box.append(int(int_x + box_size/2))

    # y2
    if int_y + box_size/2 > int(image_size[1]):
        box.append(image_size[1])
    else:
        box.append(int(int_y + box_size/2))

    box.append(class_name)
    return box

def find_boxes_from_points(coors):
    height = euclide_distance(coors[0], coors[3])
    box_size = int(0.1 * height)
    box = []
    for i, c in enumerate(coors):
        box.append(get_box((400, 250), c, i, box_size))
    return np.array(box)

def rand(a=0, b=1):
    return np.random.rand() * (b - a) + a

def super_augment(image, box, train_size):
    try:
        image = Image.fromarray(image.astype('uint8'))
        box = np.array(box)

        covered_rectangle = find_covered_rectangle(box)
        card_coors = find_card_coordinates(box)

        cropped = image.copy()
        coeffs = find_coeffs(card_coors, [(0, 0), (400, 0), (400, 250), (0, 250)])
        cropped = cropped.transform((400, 250), Image.PERSPECTIVE, coeffs, Image.BICUBIC)

        # Trapezoid augment
        if rand() < .5:
            offset_x = int(rand(1, 100))
            offset_y = int(offset_x / 2.)
            dst_coors = [(offset_x, offset_y), (400 - offset_x, offset_y), (350, 250 - offset_y), (50, 250 - offset_y)]
            coeffs = find_coeffs([(0, 0), (400, 0), (400, 250), (0, 250)], dst_coors)
            cropped = cropped.transform((400, 250), Image.PERSPECTIVE, coeffs, Image.BICUBIC)
        # Parallelogram augment
        else:
            offset_x = int(rand(10, 100))
            offset_y = int(offset_x / 2.)
            dst_coors = [(offset_x, offset_y), (350, offset_y), (400 - offset_x, 250 - offset_y), (50, 250 - offset_y)]
            coeffs = find_coeffs([(0, 0), (400, 0), (400, 250), (0, 250)], dst_coors)
            cropped = cropped.transform((400, 250), Image.PERSPECTIVE, coeffs, Image.BICUBIC)

        image = np.array(image)
        image[covered_rectangle[1]:covered_rectangle[3], covered_rectangle[0]:covered_rectangle[2]] = 255

        new_box = find_boxes_from_points(dst_coors).astype(np.float32)
        cropped, new_box = RandomRotate(180)(np.array(cropped).copy(), new_box.copy())

        covered_rectangle = find_covered_rectangle(new_box)
        cmt = np.array(cropped)[covered_rectangle[1]:covered_rectangle[3], covered_rectangle[0]:covered_rectangle[2]]

        new_box[:, 0] -= covered_rectangle[0]
        new_box[:, 1] -= covered_rectangle[1]
        new_box[:, 2] -= covered_rectangle[0]
        new_box[:, 3] -= covered_rectangle[1]

        size = int(rand(int(train_size/3), int(train_size/1.5)))
        cmt, new_box = Resize(size)(cmt.copy(), new_box.copy())

        x_r = int(rand(0, train_size - size - 1))
        y_r = int(rand(0, train_size - size - 1))

        image[y_r:y_r+size, x_r:x_r+size, :] = cmt
        new_box[:, 0] += x_r
        new_box[:, 1] += y_r
        new_box[:, 2] += x_r
        new_box[:, 3] += y_r

        if rand() < .3:
            image, new_box = RandomHSV(100, 100, 100)(image.copy(), new_box.copy())

        new_box = np.array(new_box).astype(np.int32).tolist()

        return np.array(image), np.array(new_box)
    except:
        return image, box


class Dataset(object):
    """implement Dataset here"""
    def __init__(self, dataset_type):
        self.annot_path  = cfg.TRAIN.ANNOT_PATH if dataset_type == 'train' else cfg.TEST.ANNOT_PATH
        self.input_sizes = cfg.TRAIN.INPUT_SIZE if dataset_type == 'train' else cfg.TEST.INPUT_SIZE
        self.batch_size  = cfg.TRAIN.BATCH_SIZE if dataset_type == 'train' else cfg.TEST.BATCH_SIZE
        self.data_aug    = cfg.TRAIN.DATA_AUG   if dataset_type == 'train' else cfg.TEST.DATA_AUG

        self.train_input_sizes = cfg.TRAIN.INPUT_SIZE
        self.strides = np.array(cfg.YOLO.STRIDES)
        self.classes = utils.read_class_names(cfg.YOLO.CLASSES)
        self.num_classes = len(self.classes)
        self.anchors = np.array(utils.get_anchors(cfg.YOLO.ANCHORS))
        self.anchor_per_scale = cfg.YOLO.ANCHOR_PER_SCALE
        self.max_bbox_per_scale = 150

        self.annotations = self.load_annotations(dataset_type)
        self.num_samples = len(self.annotations)
        self.num_batchs = int(np.ceil(self.num_samples / self.batch_size))
        self.batch_count = 0

    @staticmethod
    def euclide_distance(p1, p2):
        return np.sqrt((p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]))

    @staticmethod
    def gaussian_k(x0, y0, sigma, width, height):
        x = np.arange(0, width, 1, float) ## (width,)
        y = np.arange(0, height, 1, float)[:, np.newaxis] ## (height,1)
        return np.exp(-((x-x0)**2 + (y-y0)**2) / (2*sigma**2))

    @staticmethod
    def generate_hm(target_size, landmarks, image_size, s=3):
        Nlandmarks = len(landmarks)
        hm = np.zeros((target_size, target_size, Nlandmarks), dtype = np.float32)
        for i in range(Nlandmarks):
            if not np.array_equal(landmarks[i], [-1,-1]):
                x = int(landmarks[i][0] * target_size * 1.0 / image_size)
                y = int(landmarks[i][1] * target_size * 1.0 / image_size)
                hm[:,:,i] = Dataset.gaussian_k(x, y, s, target_size, target_size)
            else:
                hm[:,:,i] = np.zeros((target_size,target_size))
        return hm

    def load_annotations(self, dataset_type):
        with open(self.annot_path, 'r') as f:
            txt = f.readlines()
            annotations = [line.strip() for line in txt if len(line.strip().split()[1:]) != 0]
        np.random.shuffle(annotations)
        return annotations

    def __iter__(self):
        return self

    def __next__(self):

        with tf.device('/cpu:0'):
            self.train_input_size = random.choice(self.train_input_sizes)
            self.train_output_sizes = self.train_input_size // self.strides
            self.heatmap_sizes = self.train_output_sizes[0]
            # print(self.train_input_size)
            # print(self.train_output_sizes)
            # print(self.heatmap_sizes)
            batch_image = np.zeros((self.batch_size, self.train_input_size, self.train_input_size, 3))

            batch_label_sbbox = np.zeros((self.batch_size, self.train_output_sizes[0], self.train_output_sizes[0],
                                          self.anchor_per_scale, 5 + self.num_classes))
            batch_label_mbbox = np.zeros((self.batch_size, self.train_output_sizes[1], self.train_output_sizes[1],
                                          self.anchor_per_scale, 5 + self.num_classes))
            batch_label_lbbox = np.zeros((self.batch_size, self.train_output_sizes[2], self.train_output_sizes[2],
                                          self.anchor_per_scale, 5 + self.num_classes))

            batch_sbboxes = np.zeros((self.batch_size, self.max_bbox_per_scale, 4))
            batch_mbboxes = np.zeros((self.batch_size, self.max_bbox_per_scale, 4))
            batch_lbboxes = np.zeros((self.batch_size, self.max_bbox_per_scale, 4))

            batch_hm = np.zeros((self.batch_size, self.heatmap_sizes, self.heatmap_sizes, 4))

            num = 0
            if self.batch_count < self.num_batchs:
                while num < self.batch_size:
                    index = self.batch_count * self.batch_size + num
                    if index >= self.num_samples: index -= self.num_samples
                    annotation = self.annotations[index]
                    image, bboxes, points = self.parse_annotation(annotation)

                    label_sbbox, label_mbbox, label_lbbox, sbboxes, mbboxes, lbboxes = self.preprocess_true_boxes(bboxes)

                    heatmap = self.generate_hm(self.heatmap_sizes, points, image_size=self.train_input_size, s=np.sqrt(self.strides[0]/2))
                    
                    # print(self.train_input_size)
                    # print(heatmap.shape)

                    # _image = image.copy()
                    # print(_image.shape)
                    # for i in range(len(points)):
                    #     cv2.circle(_image, (int(points[i][0]), int(points[i][1])), 10, (255, 0, 0), 5)
                    # cv2.imshow('image', _image)
                    # cv2.imshow('image1', heatmap[:, :, 0])
                    # cv2.imshow('image2', heatmap[:, :, 1])
                    # cv2.imshow('image3', heatmap[:, :, 2])
                    # cv2.imshow('image4', heatmap[:, :, 3])
                    # cv2.waitKey(0)

                    batch_image[num, :, :, :] = image
                    batch_label_sbbox[num, :, :, :, :] = label_sbbox
                    batch_label_mbbox[num, :, :, :, :] = label_mbbox
                    batch_label_lbbox[num, :, :, :, :] = label_lbbox
                    batch_sbboxes[num, :, :] = sbboxes
                    batch_mbboxes[num, :, :] = mbboxes
                    batch_lbboxes[num, :, :] = lbboxes
                    batch_hm[num, :, :, :] = heatmap
                    num += 1
                self.batch_count += 1
                return batch_image, batch_label_sbbox, batch_label_mbbox, batch_label_lbbox, \
                       batch_sbboxes, batch_mbboxes, batch_lbboxes, batch_hm
            else:
                self.batch_count = 0
                np.random.shuffle(self.annotations)
                raise StopIteration

    def random_horizontal_flip(self, image, bboxes):

        if random.random() < 0.5:
            _, w, _ = image.shape
            image = image[:, ::-1, :]
            bboxes[:, [0,2]] = w - bboxes[:, [2,0]]

        return image, bboxes

    def random_crop(self, image, bboxes):

        if random.random() < 0.5:
            h, w, _ = image.shape
            max_bbox = np.concatenate([np.min(bboxes[:, 0:2], axis=0), np.max(bboxes[:, 2:4], axis=0)], axis=-1)

            max_l_trans = max_bbox[0]
            max_u_trans = max_bbox[1]
            max_r_trans = w - max_bbox[2]
            max_d_trans = h - max_bbox[3]

            crop_xmin = max(0, int(max_bbox[0] - random.uniform(0, max_l_trans)))
            crop_ymin = max(0, int(max_bbox[1] - random.uniform(0, max_u_trans)))
            crop_xmax = max(w, int(max_bbox[2] + random.uniform(0, max_r_trans)))
            crop_ymax = max(h, int(max_bbox[3] + random.uniform(0, max_d_trans)))

            image = image[crop_ymin : crop_ymax, crop_xmin : crop_xmax]

            bboxes[:, [0, 2]] = bboxes[:, [0, 2]] - crop_xmin
            bboxes[:, [1, 3]] = bboxes[:, [1, 3]] - crop_ymin

        return image, bboxes

    def random_translate(self, image, bboxes):

        if random.random() < 0.5:
            h, w, _ = image.shape
            max_bbox = np.concatenate([np.min(bboxes[:, 0:2], axis=0), np.max(bboxes[:, 2:4], axis=0)], axis=-1)

            max_l_trans = max_bbox[0]
            max_u_trans = max_bbox[1]
            max_r_trans = w - max_bbox[2]
            max_d_trans = h - max_bbox[3]

            tx = random.uniform(-(max_l_trans - 1), (max_r_trans - 1))
            ty = random.uniform(-(max_u_trans - 1), (max_d_trans - 1))

            M = np.array([[1, 0, tx], [0, 1, ty]])
            image = cv2.warpAffine(image, M, (w, h))

            bboxes[:, [0, 2]] = bboxes[:, [0, 2]] + tx
            bboxes[:, [1, 3]] = bboxes[:, [1, 3]] + ty

        return image, bboxes

    def parse_annotation(self, annotation):

        line = annotation.split()
        image_path = line[0]
        if not os.path.exists(image_path):
            raise KeyError("%s does not exist ... " %image_path)
        image = np.array(cv2.imread(image_path))
        height, width = image.shape[:2]
        bboxes = np.array([list(map(lambda x: int(float(x)), box.split(','))) for box in line[1:]])

        # if self.data_aug:
        #     image, bboxes = self.random_horizontal_flip(np.copy(image), np.copy(bboxes))
        #     image, bboxes = self.random_crop(np.copy(image), np.copy(bboxes))
        #     image, bboxes = self.random_translate(np.copy(image), np.copy(bboxes))

        image, bboxes = utils.image_preporcess(np.copy(image), [self.train_input_size, self.train_input_size], np.copy(bboxes))
        if self.data_aug and rand() < .5:
            image, bboxes = super_augment(image.copy(), bboxes.copy(), self.train_input_size)
        points = []
        for i in range(bboxes.shape[0]):
            x = (bboxes[i, 0] + bboxes[i, 2]) / 2
            y = (bboxes[i, 1] + bboxes[i, 3]) / 2
            points.append((x, y))
        return image / 255., bboxes, points

    def bbox_iou(self, boxes1, boxes2):

        boxes1 = np.array(boxes1)
        boxes2 = np.array(boxes2)

        boxes1_area = boxes1[..., 2] * boxes1[..., 3]
        boxes2_area = boxes2[..., 2] * boxes2[..., 3]

        boxes1 = np.concatenate([boxes1[..., :2] - boxes1[..., 2:] * 0.5,
                                boxes1[..., :2] + boxes1[..., 2:] * 0.5], axis=-1)
        boxes2 = np.concatenate([boxes2[..., :2] - boxes2[..., 2:] * 0.5,
                                boxes2[..., :2] + boxes2[..., 2:] * 0.5], axis=-1)

        left_up = np.maximum(boxes1[..., :2], boxes2[..., :2])
        right_down = np.minimum(boxes1[..., 2:], boxes2[..., 2:])

        inter_section = np.maximum(right_down - left_up, 0.0)
        inter_area = inter_section[..., 0] * inter_section[..., 1]
        union_area = boxes1_area + boxes2_area - inter_area

        return inter_area / union_area

    def preprocess_true_boxes(self, bboxes):

        label = [np.zeros((self.train_output_sizes[i], self.train_output_sizes[i], self.anchor_per_scale,
                           5 + self.num_classes)) for i in range(3)]
        bboxes_xywh = [np.zeros((self.max_bbox_per_scale, 4)) for _ in range(3)]
        bbox_count = np.zeros((3,))

        for bbox in bboxes:
            bbox_coor = bbox[:4]
            bbox_class_ind = bbox[4]

            onehot = np.zeros(self.num_classes, dtype=np.float)
            onehot[bbox_class_ind] = 1.0
            uniform_distribution = np.full(self.num_classes, 1.0 / self.num_classes)
            deta = 0.01
            smooth_onehot = onehot * (1 - deta) + deta * uniform_distribution

            bbox_xywh = np.concatenate([(bbox_coor[2:] + bbox_coor[:2]) * 0.5, bbox_coor[2:] - bbox_coor[:2]], axis=-1)
            bbox_xywh_scaled = 1.0 * bbox_xywh[np.newaxis, :] / self.strides[:, np.newaxis]

            iou = []
            exist_positive = False
            for i in range(3):
                anchors_xywh = np.zeros((self.anchor_per_scale, 4))
                anchors_xywh[:, 0:2] = np.floor(bbox_xywh_scaled[i, 0:2]).astype(np.int32) + 0.5
                anchors_xywh[:, 2:4] = self.anchors[i]

                iou_scale = self.bbox_iou(bbox_xywh_scaled[i][np.newaxis, :], anchors_xywh)
                iou.append(iou_scale)
                iou_mask = iou_scale > 0.3

                if np.any(iou_mask):
                    xind, yind = np.floor(bbox_xywh_scaled[i, 0:2]).astype(np.int32)

                    label[i][yind, xind, iou_mask, :] = 0
                    label[i][yind, xind, iou_mask, 0:4] = bbox_xywh
                    label[i][yind, xind, iou_mask, 4:5] = 1.0
                    label[i][yind, xind, iou_mask, 5:] = smooth_onehot

                    bbox_ind = int(bbox_count[i] % self.max_bbox_per_scale)
                    bboxes_xywh[i][bbox_ind, :4] = bbox_xywh
                    bbox_count[i] += 1

                    exist_positive = True

            if not exist_positive:
                best_anchor_ind = np.argmax(np.array(iou).reshape(-1), axis=-1)
                best_detect = int(best_anchor_ind / self.anchor_per_scale)
                best_anchor = int(best_anchor_ind % self.anchor_per_scale)
                xind, yind = np.floor(bbox_xywh_scaled[best_detect, 0:2]).astype(np.int32)

                label[best_detect][yind, xind, best_anchor, :] = 0
                label[best_detect][yind, xind, best_anchor, 0:4] = bbox_xywh
                label[best_detect][yind, xind, best_anchor, 4:5] = 1.0
                label[best_detect][yind, xind, best_anchor, 5:] = smooth_onehot

                bbox_ind = int(bbox_count[best_detect] % self.max_bbox_per_scale)
                bboxes_xywh[best_detect][bbox_ind, :4] = bbox_xywh
                bbox_count[best_detect] += 1
        label_sbbox, label_mbbox, label_lbbox = label
        sbboxes, mbboxes, lbboxes = bboxes_xywh
        return label_sbbox, label_mbbox, label_lbbox, sbboxes, mbboxes, lbboxes

    def __len__(self):
        return self.num_batchs




